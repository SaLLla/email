﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace Email
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введи email");
            string email = Console.ReadLine();
            // Reg Vol = new Reg(email);
            if (Valid(email))
            {
                Console.WriteLine("email: {0} принят", email);
            }
            else
            {
                Console.WriteLine("Введите корректный email.");
            }
            Console.ReadLine();
        }

        static bool Valid(string s)
        {
            Regex regex = new Regex(@"\b\w+([\.\w]+)*\w@\w((\.\w)*\w+)*\.\w{2,3}\b");
            Match match = regex.Match(s);
            return match.Success;
        }
    }

    class Reg
    {
        
    }
}
